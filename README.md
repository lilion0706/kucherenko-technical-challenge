# QA automation assignment

## Time limits

The evaluation result of the test is not linked to how much time you spend on it.

This assignment is meant to evaluate the QA Automation proficiency of full-time engineers.

**Do not mention 90 percent of everything or 90poe anywhere on the code or repository name.**

**Do not copy any files from this repository**

## Evaluation points in order of importance

- correctly defined test cases in BDD format
- readiness for CI
- edge cases are covered by tests
- correct error messages
- documentation: README and inline code comments
- use of docker


Results: please share a git repository with us containing your implementation.

Level of experience targeted: EXPERT

Choose any automation tool. Cypress is a big plus.

If you have questions please make some assumptions and collect in writing your interpretations.

Good luck.

## Technical test

Given an application(oos_ca). Run it using the instructions in README.md. Observe the application functionality, assuming that it works correctly.
Create automation framework that will make sure that behavior stays consistent in future versions.
Create documentation on how to work with your test framework.
Have at least two test suites.

## Bonus points

- Use Docker for tests
- Measure performance of different UI operations
