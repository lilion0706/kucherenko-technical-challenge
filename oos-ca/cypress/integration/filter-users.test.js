describe('Filter Users', () => {

    function filter(input, text, searchResults) {
        cy.get(input).type(text)

        cy.contains('Submit').click()

        cy.get('.App-column div')
            .find('.CrewMember-container')
            .should('have.length', searchResults)

        cy.contains('Clear').click()

        cy.get('.App-column div')
            .find('.CrewMember-container')
            .should('have.length', 5)

        cy.get(input).clear()
    }

    it('Filter Existing Name', function () {

        //for running in docker: docker-compose run crew-cypress
        cy.visit('http://crew-app:5000/')

        filter('#name', 'll', 2)
    })

    it('Filter Non-Existing Name', function () {
        filter('#name', '12345', 0)
    })

    it('Filter Existing City', function () {
        filter('#city', 'live', 1)
    })

    it('Filter Non-Existing City', function () {
        filter('#city', '12345', 0)
    })

    it('Filter by both fields (both are non-existing)', function () {
        cy.get('#name').type('12345')
        filter('#city', '12345', 0)
        cy.get('#name').clear()
    })

    it('Filter by both fields (both are existing)', function () {
        cy.get('#name').type('ll')
        filter('#city', 'here', 1)
        cy.get('#name').clear()
    })

    it('Filter by both fields (one is existing)', function () {
        cy.get('#name').type('ll')
        filter('#city', '11', 0)
        cy.get('#name').clear()
    })
});
