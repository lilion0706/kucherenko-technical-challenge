describe('Move crew members between columns', () => {
    const users = [
        {name: 'lloyd gonzalez', city: 'hereford'},
        {name: 'emma stewart', city: 'worcester'},
        {name: 'danielle moore', city: 'cardiff'},
        {name: 'linda ruiz', city: 'liverpool'},
    ];

    function moveUsers(btn) {
        users.forEach((user) => {
            cy.get('.App-column div')
                .should('contain', user.name)
                .should('contain', user.city)
                .contains('button', btn).first().click()
        });
    }

    function checkUsersInColumn(column, usersInColumn) {
        cy.get('.App-column > div')
            .contains(column)
            .nextAll()
            .should('have.length', usersInColumn)
    }

    it('Initial state', function () {

        //for running in docker: docker-compose run crew-cypress
        cy.visit('http://crew-app:5000/')

        cy.get('.App-column').contains('Applied').nextAll().should('have.length', 4)
        cy.get('.App-column').contains('Interviewing').nextAll().should('have.length', 0)
        cy.get('.App-column').contains('Hired').nextAll().should('have.length', 1)
    })

    it('Move from Applied to Interviewing', () => {
        checkUsersInColumn('Applied', 4)
        moveUsers('>')
    });

    it('Move from Interviewing to Hired', () => {
        checkUsersInColumn('Interviewing', 4)
        moveUsers('>')

        users.push({'name': "julia cunningham", 'city': 'sheffield'})
    });

    it('Move from Hired to Interviewing', () => {
        checkUsersInColumn('Hired', 5)
        moveUsers('<')
    });

    it('Move from Interviewing to Applied', () => {
        moveUsers('<')
        checkUsersInColumn('Applied', 5)
    });
});
