# CREW APPLICATION

Simple application which represents dashboard with candidates.

### Running locally
`yarn install`

`yarn start`

App will be available on http://localhost:3000


### Running in docker (new)
Requirements:
- version of docker-compose should be 1.29 or higher

Go to directory: `oos-ca`

Run application with the command:

`docker-compose up -d`


Make sure application container is up and running with command:

`docker-compose ps`

App should be available on http://localhost:5000

Run cypress tests with the following command:

`docker-compose run crew-cypress`

See video records of testing in directory `cypress/videos`

### Running in docker (old)
`docker build -t crew-app .`

`docker run -it --rm -p 5000:5000 --name crew-container crew-app`

App will be available on http://localhost:5000
